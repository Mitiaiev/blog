from flask import Flask
import os

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ["CONNECT_DB"]
app.config['SECRET_KEY'] = os.environ["SECRET_KEY"]
FOTO_FOLDER = os.environ["FOTO_FOLDER"]