from db import db, Posts, Contents, Authorization
from flask import render_template, request, url_for, flash, redirect
from flask_login import LoginManager, login_user, login_required, logout_user, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.exceptions import abort
from __init__ import app, FOTO_FOLDER
from datetime import datetime
from UserLogin import UserLogin
import hashlib

login_manager = LoginManager(app)


@login_manager.user_loader
def load_user(user_id):
    return UserLogin().fromDB(user_id, Authorization)


def get_post(post_id):
    post = Posts.query.filter(Posts.post_id == post_id).all()[0]
    if post is None:
        abort(404)
    return post


@app.route('/')
def index():
    if current_user.is_authenticated:
        user_id = current_user.get_id()
        auth = Authorization.query.filter(Authorization.id == user_id).all()[0].admin
    else:
        auth = None

    posts = Posts.query.all()
    return render_template('index.html', posts=posts, auth=auth)


@app.route('/login', methods=["POST", "GET"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("index"))

    if request.method == "POST":
        try:
            user = Authorization.query.filter(Authorization.login == request.form['name']).all()[0]
        except IndexError:
            flash("Неверная пара логин/пароль", "danger")
        else:
            if user.login and check_password_hash(user.password_hash, request.form['psw']):
                userlogin = UserLogin().create(user)
                login_user(userlogin, remember=True)
                return redirect(url_for("index"))
            else:
                flash("Неверная пара логин/пароль", "danger")

    return render_template('login.html')


@app.route('/registration', methods=["POST", "GET"])
def registration():
    if request.method == "POST":
        if len(request.form['name']) > 1 and len(request.form['email']) > 4 \
                and len(request.form['psw']) > 4 and request.form['psw'] == request.form['psw2']:
            hash = generate_password_hash(request.form['psw'])

            try:
                db.session.add(Authorization(login=request.form['name'], password_hash=hash, mail=request.form['email'],
                                             admin=False))
                db.session.commit()
            except Exception:
                flash("Ошибка при добавлении в БД", "danger")

            else:
                flash("Вы успешно зарегистрированы", "success")
                return redirect(url_for('login'))
        else:
            flash("Неверно заполнены поля, пароль должен быть не менее 5 символов", "danger")
    return render_template('registration.html')


@app.route('/<int:post_id>')
def post(post_id):
    if current_user.is_authenticated:
        user_id = current_user.get_id()
        auth = Authorization.query.filter(Authorization.id == user_id).all()[0].admin
    else:
        auth = None

    post = get_post(post_id)
    contents = Contents.query.filter(Contents.post == post.post_id).all()
    return render_template('post.html', post=post, contents=contents, foto_folder=FOTO_FOLDER, auth=auth)


@app.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if current_user.is_authenticated:
        user_id = current_user.get_id()
        auth = Authorization.query.filter(Authorization.id == user_id).all()[0].admin
    else:
        auth = None

    if request.method == 'POST':
        title = request.form['title']
        description = request.form['content']
        try:
            file = request.files['file']
        except KeyError:
            file = False
        filedesk = request.form['filedesk']

        if not title:
            flash('Title is required!')
        else:
            newPost = Posts(country=title, description=description, time_created=datetime.now())
            db.session.add(newPost)
            db.session.flush()
            db.session.refresh(newPost)
            if file:
                img = file.read()
                hash_file = hashlib.md5(img)
                file_name = f"{hash_file.hexdigest()}.png"
                with open(f"static/img/{file_name}", 'wb') as f:
                    f.write(img)
                db.session.add(Contents(foto_name=file_name, content=filedesk, post=newPost.post_id))
            db.session.commit()

            return redirect(url_for('index'))

    return render_template('create.html', auth=auth)


@app.route('/<int:id>/edit', methods=('GET', 'POST'))
@login_required
def edit(id):
    if current_user.is_authenticated:
        user_id = current_user.get_id()
        auth = Authorization.query.filter(Authorization.id == user_id).all()[0].admin
    else:
        auth = None

    post = get_post(id)

    if request.method == 'POST':
        title = request.form['title']
        content = request.form['content']

        if not title:
            flash('Title is required!')
        else:
            db.session.query(Posts).filter_by(post_id=id).update({"country": title, "description": content})
            db.session.commit()
            return redirect(url_for('index'))

    return render_template('edit.html', post=post, auth=auth)


@app.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    post = get_post(id)
    db.session.query(Contents).filter_by(post=id).delete()
    db.session.query(Posts).filter_by(post_id=id).delete()
    db.session.commit()
    flash('"{}" was successfully deleted!'.format(post.country))
    return redirect(url_for('index'))


@app.route('/logout')
def logout():
    logout_user()
    flash("Вы вышли из аккаунта", "success")
    return redirect(url_for('login'))


@app.errorhandler(404)
def page_not_found(e):
    return render_template('page404.html')


if __name__ == "__main__":
    app.run(debug=True)
