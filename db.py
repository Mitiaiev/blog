from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from __init__ import app

db = SQLAlchemy(app)
migrate = Migrate(app, db)


class Posts(db.Model):
    __tablename__ = 'posts'
    post_id = db.Column(db.Integer, primary_key=True)
    time_created = db.Column(db.DateTime, nullable=False)
    country = db.Column(db.Text, nullable=False)
    description = db.Column(db.Text, nullable=True)
    contents = db.relationship('Contents', backref='posts',
                                lazy='dynamic')


class Contents(db.Model):
    __tablename__ = 'contents'
    content_id = db.Column(db.Integer, primary_key=True)
    foto_name = db.Column(db.DateTime, nullable=True)
    content = db.Column(db.Text, nullable=False)
    post = db.Column(db.Integer, db.ForeignKey('posts.post_id'), nullable=False)


class Authorization(db.Model):
    __tablename__ = 'authorization'
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.Text, nullable=True)
    password_hash = db.Column(db.Text, nullable=False)
    admin = db.Column(db.Boolean, nullable=False)
    mail = db.Column(db.Text, nullable=True)

